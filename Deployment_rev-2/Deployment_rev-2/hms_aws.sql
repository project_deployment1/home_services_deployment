-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: hms_aws
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee_tb`
--

DROP TABLE IF EXISTS `employee_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) DEFAULT NULL,
  `emp_status` varchar(20) DEFAULT 'FREE',
  `first_name` varchar(30) DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `phone_num` bigint NOT NULL,
  `salary` double NOT NULL,
  `serviceid` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7n4xk62lpm1nchspspmvi4mhq` (`serviceid`),
  CONSTRAINT `FK7n4xk62lpm1nchspspmvi4mhq` FOREIGN KEY (`serviceid`) REFERENCES `service_tb` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_tb`
--

LOCK TABLES `employee_tb` WRITE;
/*!40000 ALTER TABLE `employee_tb` DISABLE KEYS */;
INSERT INTO `employee_tb` VALUES (1,'Painting','FREE','Harry','2022-09-03','Potter',1234569870,2100,1),(2,'Carpentry','FREE','Jhonny','2022-06-09','depp',9852364112,3000,2);
/*!40000 ALTER TABLE `employee_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_tb`
--

DROP TABLE IF EXISTS `feedback_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email_id` varchar(50) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `message` varchar(350) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_tb`
--

LOCK TABLES `feedback_tb` WRITE;
/*!40000 ALTER TABLE `feedback_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `img_tb`
--

DROP TABLE IF EXISTS `img_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `img_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `imp_size` longblob,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `img_tb`
--

LOCK TABLES `img_tb` WRITE;
/*!40000 ALTER TABLE `img_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `img_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_tb`
--

DROP TABLE IF EXISTS `orders_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bookingtime` datetime(6) NOT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `employee_id` bigint DEFAULT NULL,
  `service_id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1aakvcborhck5ls2h3foikpeg` (`employee_id`),
  KEY `FK3618m7fbv7xlid51xd5reun6q` (`service_id`),
  KEY `FKrkl8m98oxqbinrfpy6y8jghos` (`user_id`),
  CONSTRAINT `FK1aakvcborhck5ls2h3foikpeg` FOREIGN KEY (`employee_id`) REFERENCES `employee_tb` (`id`),
  CONSTRAINT `FK3618m7fbv7xlid51xd5reun6q` FOREIGN KEY (`service_id`) REFERENCES `service_tb` (`id`),
  CONSTRAINT `FKrkl8m98oxqbinrfpy6y8jghos` FOREIGN KEY (`user_id`) REFERENCES `user_tb` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_tb`
--

LOCK TABLES `orders_tb` WRITE;
/*!40000 ALTER TABLE `orders_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_img`
--

DROP TABLE IF EXISTS `service_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_img` (
  `img_id` bigint DEFAULT NULL,
  `service_id` bigint NOT NULL,
  PRIMARY KEY (`service_id`),
  KEY `FKqsdfwk2j1di8ihgwr2qsrkt14` (`img_id`),
  CONSTRAINT `FK5ifgxnjt0tc050je9663d3in8` FOREIGN KEY (`service_id`) REFERENCES `service_tb` (`id`),
  CONSTRAINT `FKqsdfwk2j1di8ihgwr2qsrkt14` FOREIGN KEY (`img_id`) REFERENCES `img_tb` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_img`
--

LOCK TABLES `service_img` WRITE;
/*!40000 ALTER TABLE `service_img` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_tb`
--

DROP TABLE IF EXISTS `service_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `long_desc` varchar(400) DEFAULT NULL,
  `service_charge` double NOT NULL,
  `service_name` varchar(50) DEFAULT NULL,
  `service_tax` double NOT NULL,
  `short_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_85hprmoj1mepemu6a3828qamo` (`service_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_tb`
--

LOCK TABLES `service_tb` WRITE;
/*!40000 ALTER TABLE `service_tb` DISABLE KEYS */;
INSERT INTO `service_tb` VALUES (1,'Our employee for painting is best for this service,they will do the job as fast as they can with superfinish in painting.',5000,'Painting',575,'Make your home beautiful as never before'),(2,'Our Carpenter never disappointed you.',3500,'Carpentry',360,'Wooden work as you never seen before.');
/*!40000 ALTER TABLE `service_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tb`
--

DROP TABLE IF EXISTS `user_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_tb` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `city` varchar(30) DEFAULT NULL,
  `house_no` varchar(50) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `street` varchar(60) DEFAULT NULL,
  `dob` datetime(6) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `password` varchar(350) NOT NULL,
  `phone` bigint NOT NULL,
  `role` varchar(40) NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jjhogmvl1sxad3l1018wkl5wr` (`phone`),
  UNIQUE KEY `UK_2dlfg6wvnxboknkp9d1h75icb` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tb`
--

LOCK TABLES `user_tb` WRITE;
/*!40000 ALTER TABLE `user_tb` DISABLE KEYS */;
INSERT INTO `user_tb` VALUES (1,'pune','C54','425314','Maharashtra','warje road','2022-02-07 05:30:00.000000','quadhome@gmail.com','Quad','Home','admin@123',9856231245,'ADMIN'),(2,'pune','54','411041','Maharashtra','Nanded phata','2023-11-09 05:30:00.000000','jack2022@gmail.com','Jack','Sparrow','jack@123',9049939842,'USER'),(3,'JALGAON','55','425001','Maharashtra','Shiv Colony','1996-10-09 05:30:00.000000','mayurmistari36@gmail.com','Mayur','Mistari','mayur@123',7972077985,'USER');
/*!40000 ALTER TABLE `user_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-23 21:43:30
